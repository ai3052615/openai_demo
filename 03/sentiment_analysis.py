from openai import OpenAI
import json
import httpx
import numpy as np

# 读取配置，在上传gitlab时配置文件ignore了
with open('../config/openai.json') as config_file:
    config = json.load(config_file)

client = OpenAI(
    base_url=config['base_url'],
    api_key=config['key'],
    http_client=httpx.Client(
        base_url=config['base_url'],
        follow_redirects=True,
    ),
)

# 指定模型
MODEL = "text-embedding-ada-002"

# 读取数据集，忽略表头行
dataset = open('dataset.csv', 'r', encoding='utf-8').readlines()[1:]
reviews = []
labels = []
for row in dataset:
    items = row.split(',')
    reviews.append(items[0])
    labels.append(items[1])


# 实现文本转embedding方法
def get_embedding(text, model=MODEL):
    return client.embeddings.create(input=[text], model=model).data[0].embedding


# 将”好评“和”差评“转换为embedding
positive = get_embedding("好评")
negative = get_embedding("差评")

# 把数据集的评论转为embedding
review_embeddings = []
for review in reviews:
    review_embeddings.append(get_embedding(review))


# 实现余弦相似度算法
def cosine_similarity(vec_a, vec_b):
    # 计算两个向量的点积
    dot_product = np.dot(vec_a, vec_b)
    # 计算两个向量的欧几里得范数（即长度）
    norm_a = np.linalg.norm(vec_a)
    norm_b = np.linalg.norm(vec_b)
    # 计算余弦相似度
    cos_similarity = dot_product / (norm_a * norm_b)
    return cos_similarity


# 计算每条评论是好评还是差评
for i in range(len(reviews)):
    print("第" + str(i) + "条评论为：" + reviews[i])
    pos_score = cosine_similarity(review_embeddings[i], positive)
    neg_score = cosine_similarity(review_embeddings[i], negative)
    print("该评论与好评的相似度为：" + str(pos_score))
    print("该评论与差评的相似度为：" + str(neg_score))
    print("判断结果为：" + ("好评" if pos_score>neg_score else "差评"))
    print("实际标签为："+labels[i])
