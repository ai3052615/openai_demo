from openai import OpenAI
import json
import httpx

# 读取配置，在上传gitlab时配置文件ignore了
with open('../config/openai.json') as config_file:
    config = json.load(config_file)

client = OpenAI(
    base_url=config['base_url'],
    api_key=config['key'],
    http_client=httpx.Client(
        base_url=config['base_url'],
        follow_redirects=True,
    ),
)

# 指定模型
MODEL = "gpt-3.5-turbo"

# 指定预定义prompt，用于提示模型生成满足要求的文本
prompt = '请你站在一个律师的角度分析用户的提问，并给出专业的回答，以下是问答过程：\n'


# 实现单次问答的方法
def get_case(question):
    completions = client.completions.create(
        model=MODEL,
        prompt=question,
        n=1
    )
    output = completions.choices[0].message['content']
    return output


# 调用方法，通过终端输入的方式进行提问
print('您好，我是一个AI法律咨询机器人，现在开始您的提问吧，您可以通过输入【exit】来结束对话：')
while True:
    user_input = input("Q：")
    if user_input.lower() == 'exit':
        print('bye~')
        break
    prompt = prompt + user_input + '\n'
    answer = get_case(prompt)
    prompt = prompt + 'A:' + answer + '\n'
    print(f"A：{answer}")
