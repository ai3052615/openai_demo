from openai import OpenAI
import json
import httpx

# 读取配置，在上传gitlab时配置文件ignore了
with open('../config/openai.json') as config_file:
    config = json.load(config_file)

client = OpenAI(
    base_url=config['base_url'],
    api_key=config['key'],
    http_client=httpx.Client(
        base_url=config['base_url'],
        follow_redirects=True,
    ),
)

# 指定模型
MODEL = "gpt-3.5-turbo"

# 指定预定义prompt，用于提示模型生成满足要求的文本
prompt = '请你站在一个律师的角度分析用户的提问，并给出专业的回答，以下是用户的提问：'


# 实现具体的问答方法
def get_case(question):
    completions = client.completions.create(
        model=MODEL,
        prompt=prompt + question,  # 将用户的提问与预定义的prompt前缀拼接起来传入
        n=1
    )
    output = completions.choices[0].message['content']
    return output


# 调用方法，通过终端输入的方式进行提问
user_input = input("请输入法律咨询问题：")
answer = get_case(user_input)
print(f"\n咨询结果：{answer}")
