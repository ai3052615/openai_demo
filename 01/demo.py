from openai import OpenAI
import httpx
import json

# 读取配置，在上传gitlab时配置文件ignore了
with open('../config/openai.json') as config_file:
    config = json.load(config_file)

client = OpenAI(
    base_url=config['base_url'],
    api_key=config['key'],
    http_client=httpx.Client(
        base_url=config['base_url'],
        follow_redirects=True,
    ),
)


def get_response(input):
    completion = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": input}
        ]
    )
    message = completion.choices[0].message.content
    return message


if __name__ == "__main__":
    user_input = input("我：")
    generated_text = get_response(user_input)
    print(f"AI：{generated_text}")
